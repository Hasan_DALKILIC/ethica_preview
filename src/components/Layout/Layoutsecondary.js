import React, { useEffect, useState } from "react";
import Cookies from 'js-cookie';
import axios from "axios";
import { useHistory } from "react-router-dom";

import "antd/dist/antd.css";
import {Layout, Button} from 'antd';
import styles from '../Layout/Layout.module.css'
import {RightSquareFilled, LeftSquareFilled } from '@ant-design/icons';
import Logo from "../logo.svg";

import Rightone from "../Rightone";
import Righttwo from "../Righttwo";
import Rightthree from "../Rightthree";
import Rightfour from "../Rightfour";

const LayoutSec = (prop) => {
    const checkLoginUrl = "https://ethica.alo-tech.com/api/?function=checklogin";
    const logoutUrl = "https://ethica.alo-tech.com/api/?function=logout";

    const [data, setData] = useState("");
    const [load, setLoad] = useState(true);
    const session = Cookies.get("session");
    const history = useHistory();
    const { Header, Sider } = Layout;


    function handleLogout() {
        axios.get(`${logoutUrl}&session=${session}`)
            .then((response) => {
                if(response.data.logout) {
                    Cookies.remove("session", { path: '' });
                    history.push("/");
                }
            })
            .catch(error => {
                console.log(error)
            })   
    }

    useEffect(() => {
        const interval = setInterval(() => {
        axios.post(`${checkLoginUrl}&session=${session}`)
            .then((response) => {
                if(!response.data.login) {
                    console.log("Giriş Başarısız")
                    handleLogout();
                } else {
                    console.log("Giriş Başarılı")
                    // getData();
                    setLoad(false)
                    }
            })
        }, 10000 );
        return () => clearInterval(interval);
    })

    return (
        <Layout>
            <Header className={styles.header}>
                <h1 className={styles.brand}>ethica</h1>
                <div className={styles.icon}>
                    <a href="/wbprimary" ><LeftSquareFilled  style={{fontSize: '2rem', color: 'white'}} /></a>
                    <a href="/wbsecondary"><RightSquareFilled  style={{fontSize: '2rem', color: 'white'}} /></a>
                    <Button ghost onClick={handleLogout} className={styles.button} >
                        Log Out
                    </Button> 
                </div> 
            </Header>
                <Layout className={styles.layoutsec}>
                    <Rightone/>
                    <Righttwo/>
                    <Rightthree/>
                    <Rightfour/>
                </Layout>
        </Layout>
    )
}

export default LayoutSec;

