import React, { useState } from 'react';
import axios from 'axios';
import { useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import qs from "querystring";

import 'antd/dist/antd.css';
import styles from '../Login/Login.module.css'
import Logo from "../logo.svg";
import AloTechLogo from "../alo.PNG";
import { Form, Input, Button, Checkbox, Row, Col, Layout } from 'antd';


const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 12 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 14 },
};
const { Header, Footer, Content } = Layout;

const Login = () => {
    const loginUrl = "https://ethica.alo-tech.com/api/?function=login&";
    const privilidgesUrl = "https://ethica.alo-tech.com/api/?function=privilidges&";
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const history = useHistory();
    
    var requestParams = {
        email: userName,
        password: password
    }
    const AuthLogin = async () => {
        try {
            const responseLogin = await axios
                .get(loginUrl + qs.stringify(requestParams))
            console.log("res  " + responseLogin.data.message)
            if (!responseLogin.data.login) {
                throw new Error()
            }
            const session = responseLogin.data.session;
            Cookies.set("session", session, {expires: 1});
            Cookies.set("username", userName, {expires: 1});
            try {
                const responsePrivilidges = await axios
                    .post(privilidgesUrl + qs.stringify({'session': session}))
                if ( !(responsePrivilidges.data.supervisor && responsePrivilidges.data.active) ) {
                    Cookies.remove("session", { path: ''});
                }   else if ( responsePrivilidges.data.supervisor && responsePrivilidges.data.active) {
                    history.push("/wbprimary");
                    console.log('session', session);
                }
            } catch (error) {  
            }
        } catch (error) {
            console.log(error.message)
        }
    }

    return (
        <Layout className={styles.layout} >
            <Header className={styles.layoutheader}>
                <div className={styles.brand}>
                <img className={styles.logo} src={Logo}/>
                </div>         
            </Header>
            <Row 
                className={styles.row}
                justify= 'center'
            >
                <Col span={7} className={styles.mainCol}>
                    <div className={styles.headerContainer}>
                        <h2 className={styles.header}>Login</h2>
                    </div>
                    <Form               
                        {...layout}
                        name="basic"
                        initialValues={{ remember: true }}
                    >
                        <Form.Item
                            label="Username"
                            name="username"
                            rules={[{ required: true, message: 'Please input your username!' }]}
                            onChange={e => setUserName(e.target.value)}
                        >
                            <Input className={styles.input}/>
                        </Form.Item>
                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[{ required: true, message: 'Please input your password!' }]}
                            onChange={e => setPassword(e.target.value)}
                        >
                            <Input.Password className={styles.input}/>
                        </Form.Item>
                        <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                            <Checkbox>Remember me</Checkbox>
                        </Form.Item>
                        <Form.Item {...tailLayout}>
                            <Button 
                                type="secondry" 
                                htmlType="submit" 
                                className={styles.button}
                                onClick={AuthLogin}
                            >
                                Sign In
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
            <Footer className={styles.layoutfooter}>   
                <div>
                    <h2>Powered By AloTech</h2>
                    <h4>© 2021 AloTech All Rights Reserved</h4>
                </div>
                <div><img className={styles.logo} src={AloTechLogo} /></div>   
            </Footer>
        </Layout>
    );
};

export default Login;