import React from 'react';

import "antd/dist/antd.css";
import { List, Card } from 'antd';
import styles from './Layout/Layout.module.css'


const Righttwo = () => {
    
    const data = [
        {
            title: 'Toplam Arama Sayısı',
        },
        {
            title: 'Müşteriye Ulaştı',
        },
        {
            title: '%',
        },
        ];

    return (
        <List
            grid={{ gutter: 8, column: 3 }}
            dataSource={data}
            renderItem={item => (
            <List.Item>
                <Card title={item.title} style={{textAlign:'center'}}>Card content</Card>
            </List.Item>
            )}
        />
    );
};

export default Righttwo;