import React, {useState, useEffect} from 'react';
import Cookies from "js-cookie";
import axios from 'axios';

import "antd/dist/antd.css";
import { List, Card } from 'antd';
import styles from './Layout/Layout.module.css'

const Rightthree = () => {
    const queuePerfUrl = "https://ethica.alo-tech.com/api/?function=reportQueuePerfSummary&startdate=2021-03-04&finishdate=2021-03-04";
    const session = Cookies.get("session");
    const [dataQueue, setDataQueue] = useState("");
    var jsonRes = {};

    useEffect(() => {
        const interval = setInterval(() => {
            getData()
        }, 10000 );
        return () => clearInterval(interval);
    })

    const getData = async () => {
        const res = await axios.get(`${queuePerfUrl}&session=${session}`)
        res.data.queuePerfSummary.map( item => {
        jsonRes[`queue_${item.queue}`] = {
            inboundCalls: item.inboundCalls,
            abandon: item.abandon,
            answered: item.answered
            }
        })
        setDataQueue(jsonRes)
    }

    try {
        var inboundCalls = dataQueue.queue_Inbound.inboundCalls;
        var abandonCalls = dataQueue.queue_Inbound.abandon; 
        var answeredCalls = dataQueue.queue_Inbound.answered;  
    } catch (error) {
        console.log("error" + error)
    }
    
    const data = [
        {
            title: 'Gelen Çağrı Sayısı',
            content: inboundCalls,
        },
        {
            title: 'Kaçan',
            content: abandonCalls,
        },
        {
            title: 'Cevaplanan',
            content: answeredCalls,
        },
        {
            title: 'Bekleyen',
            content: "-"
        },
        ];

    return (
        <List
            grid={{ gutter: 8, column: 4 }}
            dataSource={data}
            renderItem={item => (
            <List.Item>
                <Card title={item.title} style={{textAlign:'center'}}>{item.content}</Card>
            </List.Item>
        )}
        />
    );
};

export default Rightthree;