import React, {useState, useEffect} from 'react';
import Cookies from "js-cookie";
import axios from 'axios';

import "antd/dist/antd.css";
import { List, Card } from 'antd';
import styles from './Layout/Layout.module.css'

const Rightfour = () => {
    const chatQueuUrl = "https://ethica.alo-tech.com/api/?function=monitoring_queue_chat";
    const chatWaitUrl = "https://ethica.alo-tech.com/api/?function=get_waiting_chats";
    const session = Cookies.get("session");
    const [dataChat, setDataChat] = useState("");
    const [dataWait, setWaitData] = useState("");

    useEffect(() => {
        const interval = setInterval(() => {
            getData()
        }, 10000 );
        return () => clearInterval(interval);
    })

    const getData = async () => {
        const resQueue = await axios.get(`${chatQueuUrl}&session=${session}`)
        console.log("response  " + resQueue.data)
        const response = resQueue.data.queuestatistics.map( item => {
            return {
                chat_accept: item.chat_accept,
                park_count: item.park_count,
                chat_reject: item.chat_reject,    
            }
        })

        const res = await axios.get(`${chatWaitUrl}&session=${session}`)
        const responseWait = res.data.stats.waiting_chats.map( item => {
            return {
                wait_count: item.waiting_count,
            }
        })
        setDataChat(response);  
        setWaitData(res);  
    }
  
    var total_chat_accept = 0;
    var total_park_count = 0;
    var total_chat_reject = 0;
    var total_wait_count = 0;
 

    for (let i = 0; i < dataChat.length; i++) {
        total_chat_accept += dataChat[i].chat_accept;
        total_park_count += dataChat[i].park_count; 
        total_chat_reject += dataChat[i].chat_reject;       
    }
    for (let i = 0; i < dataWait.length; i++) {
        total_wait_count += dataWait[i].wait_count;
    }

    var total_chat = total_chat_accept + total_wait_count + total_chat_reject

    console.log("mer  " + total_chat_accept)
    const data = [
        {
            title: 'Gelen Chat Sayısı(Tüm Chat Kanalları)',
            content: total_chat,
        },
        {
            title: 'Kaçan',
            content: total_chat_reject,
        },
        {
            title: 'Park',
            content: total_park_count,
        },
        {
            title: 'Kapanan',
            content: total_chat_accept,
        },
        {
            title: 'Bekleyen',
            content: total_wait_count,
        },
        ];
    return (
        <List
            grid={{ gutter: 8, column: 5 }}
            dataSource={data}
            renderItem={item => (
            <List.Item>
                <Card title={item.title} style={{textAlign:'center'}}>{item.content}</Card>
            </List.Item>
            )}
        />
    );
};

export default Rightfour;