import React, {useState, useEffect} from 'react';
import Cookies from "js-cookie";
import axios from 'axios';

import "antd/dist/antd.css";
import { List, Card } from 'antd';
import styles from './Layout/Layout.module.css'

const Rightone = () => {
    const dialerCampUrl = "https://ethica.alo-tech.com/api/?function=campaignlistdialed";
    const campId = "ahRzfm11c3RlcmktaGl6bWV0bGVyaXIlCxIIQ2FtcGFpZ24iF0VzdGV0aGljYVdlYkNybUNhbXBhaWduDKIBE2V0aGljYS5hbG8tdGVjaC5jb20"

    const session = Cookies.get("session");
    const [dataCamp, setDataCamp] = useState("");

    useEffect(() => {
        const interval = setInterval(() => {
            getData()
        }, 10000 );
        return () => clearInterval(interval);
    })

    const getData = async () => {
        try {
            const res = await axios.get(`${dialerCampUrl}&campaignkey=${campId}&session=${session}`)
            var response = res.data.list.map( item => {
                return {
                    remainingData: parseInt(item.remainingData),
                    dialedCount: parseInt(item.dialedCount),
                    untouched: parseInt(item.untouched),
                    row: parseInt(item.row),
                }
            })
        } catch (error) {
            console.log("error" + error)
        }
        setDataCamp(response)    
    }

    var total_remainingData = 0;
    var total_dialedCount = 0;
    var total_untouched = 0;
    var total_row = 0;

    for (let i = 0; i < dataCamp.length; i++) {
        total_remainingData += dataCamp[i].remainingData;
        total_dialedCount += dataCamp[i].dialedCount;
        total_untouched += dataCamp[i].untouched;
        total_row += dataCamp[i].row; 
    }

    const data = [
    {
        title: 'Lead (Kampanyaya Yüklenen ve Aktif Aranacak Data Sayısı-tekil)',
        content: total_row,
    },
    {
        title: 'Dokunulmamış Data',
        content: total_untouched,
    },
    {
        title: 'Aranmayı Bekleyen',
        content: total_remainingData,
    },
    {
        title: 'Completed(Tam',
        content: total_dialedCount,
    },
    {
        title: 'Abandon(Dialer)',
        content: "-",
    },
    ];

    return (
        <List
            grid={{ gutter: 8, column: 5 }}
            dataSource={data}
            renderItem={item => (
            <List.Item>
                <Card title={item.title} style={{textAlign:'center'}}>{item.content}</Card>
            </List.Item>
            )}
        />
    );
};

export default Rightone;