import React, { useEffect, useState } from "react";
import { Table, Tag } from 'antd';
import axios from "axios";
import Cookies from 'js-cookie';

import "antd/dist/antd.css";
import './MtTable.css';


function MtTable() {

  function convert(time) {
    var c_minute = Math.floor(time / 60);
    var c_second = Math.floor(time % 60);
    var c_hour = Math.floor(c_minute / 60);
    c_minute = Math.floor(c_minute % 60);
    if (c_second < 10) {
      c_second = '0' + c_second;
    }
    if (c_minute < 10) {
      c_minute = '0' + c_minute;
    }
    if (c_hour < 10) {
      c_hour = '0' + c_hour;
    }
    var ctime = c_minute + ':' + c_second;
    return ctime;
  }
  const session = Cookies.get("session");
  const [state, setState] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const interval = setInterval(() => {
      getData()
    }, 1000);
    return () => clearInterval(interval);


  }, [])
  const getData = async () => {
    const res = await axios({ url: 'https://ethica.alo-tech.com/api/?function=monitoringAgentStatistic2', method: "GET", params: { session: session } })
    const data = [];
    const response = res.data
    for (var i = 0; i < response.agentstatus.length; i++) {
      for (var j = 0; j < response.agentstatistics.length; j++) {
        if (response.agentstatus[i].agent === response.agentstatistics[j].agent) {
          if (response.agentstatus[i].status !== "logoff") {
            var inbound_calls = response.agentstatistics[j].inboundcalls;
            var outbound_calls = response.agentstatistics[j].outboundcalls;
            var efficiency = response.agentstatistics[j].efficiency;
            var total_break = convert(response.agentstatistics[j].totalbreak);
            var total_extra = convert(response.agentstatistics[j].totalextra);

            data.push({
              'name': (response.agentstatus[i].agentname).toLowerCase(),
              'statu': response.agentstatus[i].status,
              'direction': response.agentstatus[i].call_direction,
              'status_update': convert(response.agentstatus[i].statusupdate),
              'inbound_calls': inbound_calls,
              'outbound_calls': outbound_calls,
              'efficiency': efficiency,
              'total_break': total_break,
              'total_extra': total_extra,
            })
          }
        }
      }
    }
    setState(data);
    setLoading(false);
  }


  const sorter = (a, b) => (isNaN(a) && isNaN(b) ? (a || '').localeCompare(b || '') : a - b);

  const columns = [
    {

      title: 'Agent',
      dataIndex: 'name',
      key: 'name',
      sortDirections: ['descend', 'ascend'],
      sorter: (a, b) => sorter(a.name, b.name),
    },
    {

      title: 'Statü',
      dataIndex: 'statu',
      key: 'statu',
      sorter: (a, b) => sorter(a.statu, b.statu),
      render: tag => {
        let color = '';
        if (tag === "available") {
          color = 'green';
        }
        if (tag === 'shortbreak') {
          color = 'volcano';
        }
        if (tag === 'lauch') {
          color = 'blue';
        }
        if (tag === 'wrapup') {
          color = 'volcano';
        }
        if (tag === 'talking') {
          color = 'geekblue';
        }; return (
          <>
            <Tag color={color} key={tag}>
              {tag}
            </Tag>

          </>
        )
      },
    },
    {
      title: 'Süre',
      dataIndex: 'status_update',
      key: 'status_update',
    },
    {
      title: 'Verimlilik Brüt	',
      dataIndex: 'inbound_calls',
      key: 'inbound_calls'
    },
    {
      title: 'Verimlilik Net	',
      dataIndex: 'outbound_calls',
      key: 'outbound_calls'
    },
    // {
    //   title: '%',
    //   dataIndex: 'efficiency',
    //   key: 'efficiency'
    // },
    // {
    //   title: 'Total Break',
    //   dataIndex: 'total_break',
    //   key: 'total_break'
    // },
    // {
    //   title: 'Total Extra',
    //   dataIndex: 'total_extra',
    //   key: 'total_extra'
    // },
  ];
  
  return (
    <div className="data_table">
      {loading ? (
        "Loading"
      ) : 
      (
          <Table pagination={{ defaultPageSize: 10 }} columns={columns} dataSource={state} />
      )}
    </div>
  );
}

export default MtTable;