import { 
  BrowserRouter as Router,
  Switch,
  Route
  } from "react-router-dom";

import Login from "./components/Login/Login";
import Layoutprimary from "./components/Layout/Layoutprimary";
import Layoutsecondary from "./components/Layout/Layoutsecondary";




function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Login/>
        </Route>
        <Route path="/wbprimary" exact>
          <Layoutprimary/>
        </Route>
        <Route path="/wbsecondary" exact>
          <Layoutsecondary/>
        </Route>
      </Switch>
    </Router> 
  );
}

export default App;
